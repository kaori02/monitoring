#pragma once

#include <vector>

#include "ofMain.h"
#include "manager.h"
#include "robot.h"

// fungsi utama, yang mengatur jalannya pertandingan
class Game : public ofBaseApp
{
private:

    // mengatur pengiriman data
    Manager manager_;

    // sekumpulan robot dalam bentuk vector of robot pointer
    std::vector<Robot*> robots_;

	void addRobotMessage (Robot* robot, int new_value, int key, int delta);

public:

    // dilakukan di awal
    void setup ();
    // dilakukan setiap loop
    void update ();
    // dilakukan untuk menggambar
    void draw ();
};