#include "game.h"
#include "message.h"

// dilakukan di awal
void Game::setup ()
{
    // mengkosongkan data pada vector robot
    robots_.clear();

}

UDPServer udp_server(7979);

// dilakukan setiap loop
void Game::update ()
{
	char message[999999];

    if(int len = udp_server.receive(message, 999999)){

        float hasil, px, py, rot;
        printf("%d ", len);
        
        for(int i=0; i<len; i++)
        {
            if(i<=3)
                cout<<message[i]<<" ";

            else if(i<=7){
                if(i<=5) cout<<(int)message[i]<<" ";
                else if(i==6) cout<<"team id= "<<(int)message[i]<<" ";
                else if(i==7) cout<<"robot id= "<<(int)message[i]<<" ";
            }

            else{
                hasil = (message[i+1] | message[i]<<8);

                cout<<hasil<<" ";
                
                if(hasil==256){
                    px = (message[i+2]<<8 | message[i+3]);
                    printf("x= %.2f ", px);
                }
                else if(hasil==257){
                    py = (message[i+2]<<8 | message[i+3]);
                    printf("y= %.2f ", py);  
                }
                else if(hasil==258){
                    rot = (message[i+2]<<8 | message[i+3]);
                    rot /= 1000;
                    printf("rot= %.2f ", rot);
                }
                i += 3;
            }
            if(message[7] == 0){
                robots_.push_back(new Robot (px, py, rot, 0, 0));

            }

            else if(message[7] == 1){
                robots_.push_back(new Robot (px, py,rot, 0, 1));

            }

            else if(message[7] == 2){
                robots_.push_back(new Robot (px, py, rot, 0, 2));
            }


        }
        puts("");   
    }
}

// dilakukan untuk menggambar
void Game::draw ()
{
    // menggambar background hijau
    ofSetColor(79, 193, 67);            // set warna hijau
    ofFill();                           // set draw dengan fill
    ofDrawRectangle(0, 0, 900, 600);    // gambar background hijau

    // untuk masing masing robot
    for (auto &robot : robots_)
    {
        // gambar robot
        robot->draw();
    }
}

// menambahkan pesan dari robot jika perbedaan nilai sebelumnya dan nilai baru lebih dari delta
void Game::addRobotMessage (Robot* robot, int new_value, int key, int delta)
{
	if (abs (new_value - robot->prev_data_[key]) > delta)
	{
		manager_.addMessage (key, new_value);
		robot->prev_data_[key] = new_value;
	}
}