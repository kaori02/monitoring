#include <math.h>

#include "base.h"

// constructor
Base::Base (float position_x, float position_y, float rotation)
{
    // inisialisasi parameter
    position_x_ = position_x;
    position_y_ = position_y;
    rotation_ = rotation;
}

// destructor
Base::~Base ()
{

}

// fungsi untuk menentukan jarak antara suatu koordinat dengan objek ini
float Base::positionDistance (float target_x, float target_y)
{
    // rumus trigonometri
    return pow(pow(target_x - position_x_, 2) + pow(target_y - position_y_, 2) , 0.5);
}

// fungsi untuk menentukan sudut antara suatu koordinat dengan objek ini
float Base::positionAngle (float target_x, float target_y)
{
    // atan = invers dari tan
    float target_angle = atan2(target_y - position_y_, target_x - position_x_);
    return clampAngle(target_angle);
}

// membuat nilai sudut selalu di antara -pi < angle < pi
float Base::clampAngle (float angle)
{
    while (angle < -PI) angle += 2 * PI;
    while (angle > PI) angle -= 2 * PI;

    return angle;
}