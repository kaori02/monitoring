#pragma once

#include <map>

#include "base.h"

// nilai key untuk parameter pesan
enum RobotKey
{
	ROBOT_POSITION_X = 0x0100,
	ROBOT_POSITION_Y = 0x0101,
	ROBOT_ROTATION = 0x0102,
};

// class yang mewakili robot, diturunkan dari class base
class Robot : public Base
{
private:

    // kecepatan gerak
    float move_speed_ = 30;
	//kecepatan rotasi
	float rotate_speed_ = 1;

    // tujuan posisi dari pergerakan
    float target_x_ = 0;
    float target_y_ = 0;

public:

	// digunakan untuk menyimpan data sebelumnya
	// berbentuk map, terdiri dari key, value
	std::map<uint16_t, int> prev_data_;

    int team_id_; // id tim
    int robot_id_; // id robot

    // constructor destructor
    Robot (float position_x, float position_y, float rotation, int team_id, int robot_id);
    ~Robot ();

    // dilakukan setiap loop
    void update ();
    // dilakukan untuk menggambar
    void draw ();

    // fungsi untuk bergerak (mengubah posisi)
    void move ();

	// fungsi untuk rotasi (mengubah rotasi)
	void rotate ();
};