#pragma once

#include <vector>

// structur dari parameter pada pesan
struct MessageValue
{
    // key = kunci / index / alamat
    uint16_t key_ = 0;

    // value = nilai
    uint16_t value_ = 0;
};

// setructur dari pesan
struct Message
{
    // header, digunakan untuk memastikan data yang dikirim merupakan pesan dari program ini
    uint8_t message_header_[4] = {'M', 'X', 'T', 'C'};

    // versi protocol pada pesan
    uint8_t message_version_ = 2;

    // panjang dari pesan, dalam hal ini jumlah dari message value
    uint8_t message_length_ = 0;

    // id team dari robot pengirim
    uint8_t team_id_ = 0;

    // id robot pengirim
    uint8_t robot_id_ = 0;

    // sekumpulan parameter pesan, vector of message value
    std::vector<MessageValue> message_values_;
};