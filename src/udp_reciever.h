#pragma once

#include "ofxNetwork.h"

class UDPServer{
    private:
        ofxUDPManager udp_manager_;

    public:
        UDPServer (int port);
        ~UDPServer ();

        // void send (const char* message, int message_length);
        int receive(char* message, int message_length);

};
