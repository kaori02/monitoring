#pragma once

#include "udp_reciever.h"
#include "message.h"

// class yang mengatur pengiriman pesan
class Manager
{
private:

    // udpserver digunakan untuk mengatur manajemen jaringan udp
    UDPServer udp_server_(int port);

    // pesan yang akan dikirim
    Message message_;

    // buffer data yang akan terkirim ke jaringan udp
    char buffer_[2048]; // string
    int buffer_length_ = 0; // panjang dari string

    // menambahkan sebuah byte ke dalam buffer
    void buildUInt8 (uint8_t value, int &pos);

    // menambahkan dua buah byte ke dalam buffer
    void buildUInt16 (uint16_t value, int &pos);

public:

    // constructor destructor
    Manager ();
    ~Manager ();

    // mengkosongkan pesan
    void clearMessage ();

    // mengatur ulang pesan
    void setupMessage (int team_id, int robot_id);

    // menambahkan parameter pesan pada pesan
    void addMessage (int key, int value);

    // mengubah pesan ke dalam bentuk string
    void buildMessage ();

    // mengirim pesan ke jaringan udp
    void receiveMessage ();
};