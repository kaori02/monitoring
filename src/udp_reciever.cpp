#include "udp_reciever.h"

UDPServer::UDPServer(int port){

    udp_manager_.Create();
    // udp_manager_.SetEnableBroadcast(true);
    // udp_manager_.Connect(broadcast_ip, port);

    udp_manager_.Bind(port);
    udp_manager_.SetNonBlocking(true);
}

UDPServer::~UDPServer(){
    udp_manager_.Close();
}

int UDPServer::receive(char* message, int message_length){

    return udp_manager_.Receive(message, message_length);
}