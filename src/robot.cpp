#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "ofMain.h"
#include "game.h"

#include "robot.h"



// constructor
Robot::Robot (float position_x, float position_y, float rotation, int team_id, int robot_id) : Base (position_x, position_y, rotation)
{
    // set parameter
    team_id_ = team_id;
    robot_id_ = robot_id;

	target_x_ = position_x;
	target_y_ = position_y;
}

// destructor
Robot::~Robot ()
{

}

// dilakukan untuk menggambar
void Robot::draw ()
{
    // push dan pop bekerja layaknya { } pada program
    ofPushMatrix();
    // mentranslasi (memindahkan) lingkup menggambar ke posisi x y
    ofTranslate(position_x_, position_y_);
    ofPushMatrix();
    // merotasi (memutar) lingkup menggambar sebesar rotation_
    ofRotateRad(rotation_);
    // gambar sebuah lingkaran dengan lingkaran kecil
    ofSetColor(128, 0, 0); // set warna merah
    ofDrawCircle(0, 0, 32); // lingkaran besar
    ofSetColor(0, 0, 0); // set warna hitam
    ofDrawCircle(16, 0, 16); // lingkaran keci
    ofPopMatrix();
    ofPopMatrix();
}

// fungsi untuk bergerak (mengubah posisi dan rotasi)
void Robot::move ()
{
    // berpindah menuju target, sejauh move speed
    // ofGetElapsedTimef digunakan untuk memperhalus pergerakan
    position_x_ += move_speed_ * cos(rotation_) * ofGetElapsedTimef();
    position_y_ += move_speed_ * sin(rotation_) * ofGetElapsedTimef();
}

// fungsi untuk bergerak (mengubah posisi dan rotasi)
void Robot::rotate ()
{
	// menghitung perbedaan sudut target dan sudut saat ini
    // ofGetElapsedTimef digunakan untuk memperhalus pergerakan
	float angle = clampAngle(positionAngle (target_x_, target_y_) - rotation_);
	if (angle > 0)
	{
		rotation_ += rotate_speed_ * ofGetElapsedTimef ();
	}
	else
	{
		rotation_ -= rotate_speed_ * ofGetElapsedTimef ();
	}
}