#include <stdio.h>
#include "udp_reciever.h"
#include "manager.h"

// constructor
Manager::Manager (){

}

// destructor
Manager::~Manager ()
{

}

// mengkosongkan pesan
void Manager::clearMessage ()
{
    // kosongkan vector
    message_.message_values_.clear();
    // reset nilai
    message_.message_length_ = 0;
}

// mengatur ulang pesan
void Manager::setupMessage (int team_id, int robot_id)
{
    // memanggil fungsi untuk mengkosongkan pesan
    clearMessage ();

    // mengubah parameter pada pesan
    message_.team_id_ = team_id;
    message_.robot_id_ = robot_id;
}

// menambahkan parameter pesan pada pesan
void Manager::addMessage (int key, int value)
{
    // membuat parameter pesan, atur nilainya
    MessageValue message_value;
    message_value.key_ = key;
    message_value.value_ = value;

    // tambahkan parameter pesan ke pesan
    message_.message_values_.push_back(message_value);

    // tambah juga ukuran dari pesan
    message_.message_length_ = message_.message_values_.size();
}

// mengubah pesan ke dalam bentuk string
// pesan akan terbentuk secara urut, dari byte pertama adalah message header
// dan seterusnya sampai byte terakhir adalah parameter pesan
void Manager::buildMessage ()
{
    int pos = 0;

    // mengubah header ke dalam bentuk string
    for (int i = 0; i < 4; i++)
    {
        buildUInt8(message_.message_header_[i], pos);
    }

    buildUInt8(message_.message_version_, pos);
    buildUInt8(message_.message_length_, pos);
    buildUInt8(message_.team_id_, pos);
    buildUInt8(message_.robot_id_, pos);

    // untuk masing masing parameter pesan
    for (int i = 0; i < message_.message_length_; i++)
    {
        buildUInt16(message_.message_values_[i].key_, pos);
        buildUInt16(message_.message_values_[i].value_, pos);
    }

    // debug

}

// menambahkan sebuah byte ke dalam buffer
// satu byte bernilai 0-225
void Manager::buildUInt8 (uint8_t value, int &pos)
{
    // menambahkan sebuah nilai pada buffer
    buffer_[pos++] = (char)value; // char = byte

    // mengubah panjang buffer
    buffer_length_ = pos;
}

// menambahkan dua buah byte ke dalam buffer
// untuk angka yang lebih besar satu byte tidak akan cukup
// tetapi pesan hanya bisa dikirim dalam bentuk byte
// untuk itu perlu menggunakan dua byte
// dengan dua byte maka nilainya 0-65535
// misal 600 dalam hexadecimal adalah 0x0258 (225 = 0xFF)
// maka nilai diatas akan dipecah menjadi dua, 0x02 dan 0x58
void Manager::buildUInt16 (uint16_t value, int &pos)
{
    // menambahkan sebuah byte ke dalam buffer
    // menggeser bit ke kanan sebanyak 8 kali (setara dengan 1 kali pada byte)
    // misal 01101101 >> 4 = 00000110
    // atau 0x0258 >> 8 = 0x0002 = 0x02
    buildUInt8(value >> 8, pos); 

    // menambahkan sebuah byte ke dalam buffer
    // melakukan operasi dan pada setiap bit
    // misal 01101101 && 00001111 = 00001101
    // atau 0x0258 && 0xFF = 0x0058 = 0x58
    buildUInt8(value & 0xFF, pos);
}

// mengirim pesan ke jaringan udp


void Manager::receiveMessage ()
{
    UDPServer udp_server(7979);
	char message[999999];
	udp_server.receive(message, 999999);
	
}