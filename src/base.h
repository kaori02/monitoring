#pragma once

#ifndef PI
#define PI 3.14159265359
#endif

// class yang mewakili transformasi dari object
class Base
{
    public:
        float position_x_; // koordinat x
        float position_y_; // koordinat y
        float rotation_; // rotasi dalam radian

        // constructor destructor
        Base (float position_x, float position_y, float rotation);
        ~Base ();

        // fungsi untuk menentukan jarak antara suatu koordinat dengan objek ini
        float positionDistance (float target_x, float target_y);
        // fungsi untuk menentukan sudut antara suatu koordinat dengan objek ini
        float positionAngle (float target_x, float target_y);
        // membuat nilai sudut selalu di antara -pi < angle < pi
        float clampAngle (float angle);
};